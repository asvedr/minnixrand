pub mod error;
pub mod function;
pub mod marsaglia;
pub mod predictable;
mod proto;
pub mod urandom;

pub use proto::IRandomizer;
