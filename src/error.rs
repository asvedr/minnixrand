use std::io::Error;

#[derive(Debug)]
pub enum RandomizerError {
    IO(Error),
}

impl From<Error> for RandomizerError {
    fn from(value: Error) -> Self {
        Self::IO(value)
    }
}
