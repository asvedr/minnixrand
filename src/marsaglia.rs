use std::cell::RefCell;
use std::time::{SystemTime, UNIX_EPOCH};

use crate::IRandomizer;

pub struct MarsagliaRandom {
    state: RefCell<State>,
}

struct State {
    u: usize,
    v: usize,
}

impl Default for MarsagliaRandom {
    fn default() -> Self {
        Self::new()
    }
}

impl MarsagliaRandom {
    pub fn new() -> Self {
        let result = Self {
            state: RefCell::new(State { u: 0, v: 0 }),
        };
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs() as usize;
        result.init(now, now - 10);
        result
    }

    pub fn init(&self, u: usize, v: usize) {
        self.state.borrow_mut().init(u, v);
    }
}

impl State {
    fn init(&mut self, u: usize, v: usize) {
        self.u = u;
        self.v = v;
    }

    fn gen(&mut self) -> usize {
        self.v = 36969 * (self.v & 65535) + (self.v >> 16);
        self.u = 18000 * (self.u & 65535) + (self.u >> 16);
        (self.v << 16) + (self.u & 65535)
    }
}

impl IRandomizer<usize> for MarsagliaRandom {
    fn gen_pure(&self) -> usize {
        self.state.borrow_mut().gen()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::{HashMap, HashSet};

    #[test]
    fn test_not_repeat() {
        let rnd = MarsagliaRandom::new();
        rnd.init(10, 20);
        let set = (0..10000).map(|_| rnd.gen_pure()).collect::<HashSet<_>>();
        assert_eq!(set.len(), 10000);
    }

    #[test]
    fn test_prob() {
        let rnd = MarsagliaRandom::new();
        rnd.init(10, 20);
        let mut map = HashMap::new();
        let count = 10_000;
        for _ in 0..count {
            let key = rnd.gen_pure() % 100;
            let val = *map.get(&key).unwrap_or(&0.0);
            map.insert(key, val + 1.0);
        }
        let mut probs = map
            .into_iter()
            .map(|(k, v)| v / (count as f64))
            .collect::<Vec<_>>();
        probs.sort_by_key(|f| (f * 10000.0) as isize);
        let diff = (probs.first().unwrap() - probs.last().unwrap()).abs();
        assert!(diff < 0.01);
    }
}
