use crate::error::RandomizerError;

pub trait IRandomizer<T> {
    /// Method for IO generators
    fn gen(&self) -> Result<T, RandomizerError> {
        Ok(self.gen_pure())
    }

    /// Method for non IO generators
    fn gen_pure(&self) -> T {
        self.gen().unwrap()
    }
}
