use std::cell::RefCell;

use crate::IRandomizer;

pub struct FiniteRandom<T> {
    result: RefCell<Vec<T>>,
}

pub struct InfiniteRandom<T> {
    result: RefCell<Vec<T>>,
    default: T,
}

impl<T> FiniteRandom<T> {
    pub fn new(result: Vec<T>) -> Self {
        Self {
            result: RefCell::new(result),
        }
    }
}

impl<T: Clone> InfiniteRandom<T> {
    pub fn new(result: Vec<T>, default: T) -> Self {
        Self {
            result: RefCell::new(result),
            default,
        }
    }
}

impl<T> IRandomizer<T> for FiniteRandom<T> {
    fn gen_pure(&self) -> T {
        let mut ptr = self.result.borrow_mut();
        if ptr.is_empty() {
            panic!("Finite random: result exceed")
        }
        ptr.remove(0)
    }
}

impl<T: Clone> IRandomizer<T> for InfiniteRandom<T> {
    fn gen_pure(&self) -> T {
        let mut ptr = self.result.borrow_mut();
        if ptr.is_empty() {
            return self.default.clone();
        }
        ptr.remove(0)
    }
}

#[cfg(test)]
mod tests {
    use crate::predictable::{FiniteRandom, InfiniteRandom};
    use crate::IRandomizer;

    #[test]
    fn test_finite_ok() {
        let randomizer = FiniteRandom::new(vec!["a", "b", "c"]);
        assert_eq!(randomizer.gen_pure(), "a");
        assert_eq!(randomizer.gen_pure(), "b");
        assert_eq!(randomizer.gen_pure(), "c");
    }

    #[test]
    #[should_panic]
    fn test_finite_fail() {
        let randomizer = FiniteRandom::new(vec!["a", "b", "c"]);
        randomizer.gen_pure();
        randomizer.gen_pure();
        randomizer.gen_pure();
        randomizer.gen_pure();
    }

    #[test]
    fn test_infinite() {
        let randomizer = InfiniteRandom::new(vec!["a", "b", "c"], "d");
        assert_eq!(randomizer.gen_pure(), "a");
        assert_eq!(randomizer.gen_pure(), "b");
        assert_eq!(randomizer.gen_pure(), "c");
        assert_eq!(randomizer.gen_pure(), "d");
        assert_eq!(randomizer.gen_pure(), "d");
        assert_eq!(randomizer.gen_pure(), "d");
    }
}
