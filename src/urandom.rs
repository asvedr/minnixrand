use std::cell::RefCell;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;

use crate::error::RandomizerError;
use crate::IRandomizer;

const FAST: &str = "/dev/urandom";
const SECURE: &str = "/dev/random";

const F32_MAX_I: F32IntType = 10000;
const F32_MAX_F: f32 = F32_MAX_I as f32;
const F64_MAX_I: F64IntType = 10000;
const F64_MAX_F: f64 = F64_MAX_I as f64;

type F32IntType = u16;
type F64IntType = u16;

pub struct Randomizer {
    source: RefCell<File>,
}

impl Randomizer {
    pub fn fast() -> io::Result<Self> {
        Self::custom(FAST)
    }

    pub fn secure() -> io::Result<Self> {
        Self::custom(SECURE)
    }

    pub fn custom<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Ok(Self {
            source: RefCell::new(File::open(path)?),
        })
    }
}

macro_rules! impl_bytes {
    ($tp:ty) => {
        impl IRandomizer<$tp> for Randomizer {
            fn gen(&self) -> Result<$tp, RandomizerError> {
                let zero: $tp = 0;
                let mut buf = zero.to_le_bytes();
                self.source.borrow_mut().read_exact(&mut buf)?;
                Ok(<$tp>::from_le_bytes(buf))
            }
        }
    };
}

impl_bytes!(usize);
impl_bytes!(isize);
impl_bytes!(u64);
impl_bytes!(i64);
impl_bytes!(u32);
impl_bytes!(i32);
impl_bytes!(u16);
impl_bytes!(i16);
impl_bytes!(u8);
impl_bytes!(i8);

impl IRandomizer<f64> for Randomizer {
    fn gen(&self) -> Result<f64, RandomizerError> {
        let num: F64IntType = self.gen()?;
        Ok((num % F64_MAX_I) as f64 / F64_MAX_F)
    }
}

impl IRandomizer<f32> for Randomizer {
    fn gen(&self) -> Result<f32, RandomizerError> {
        let num: F32IntType = self.gen()?;
        Ok((num % F32_MAX_I) as f32 / F32_MAX_F)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_COUNT: usize = 1000;

    #[test]
    fn test_f64() {
        let randomizer = Randomizer::fast().unwrap();
        for _ in 0..TEST_COUNT {
            let val: f64 = randomizer.gen_pure();
            assert!(val >= 0.0 && val <= 1.0);
        }
    }

    #[test]
    fn test_usize() {
        let randomizer = Randomizer::fast().unwrap();
        for _ in 0..TEST_COUNT {
            let res: Result<usize, _> = randomizer.gen();
            assert!(res.is_ok())
        }
    }
}
