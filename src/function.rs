use std::cell::RefCell;

use crate::IRandomizer;

pub struct Randomizer<R, Seed, F: Fn(&Seed) -> (R, Seed)> {
    seed: RefCell<Seed>,
    func: F,
}

impl<R, Seed, F: Fn(&Seed) -> (R, Seed)> Randomizer<R, Seed, F> {
    pub fn new(seed: Seed, func: F) -> Self {
        Self {
            seed: RefCell::new(seed),
            func,
        }
    }
}

impl<R, Seed, F: Fn(&Seed) -> (R, Seed)> IRandomizer<R> for Randomizer<R, Seed, F> {
    fn gen_pure(&self) -> R {
        let mut ptr = self.seed.borrow_mut();
        let (res, new_seed) = (self.func)(&*ptr);
        *ptr = new_seed;
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn inc(val: &usize) -> (usize, usize) {
        (val % 5, val + 1)
    }

    #[test]
    fn test_inc() {
        let randomizer = Randomizer::new(1, &inc);
        assert_eq!(randomizer.gen_pure(), 1);
        assert_eq!(randomizer.gen_pure(), 2);
        assert_eq!(randomizer.gen_pure(), 3);
        assert_eq!(randomizer.gen_pure(), 4);
        assert_eq!(randomizer.gen_pure(), 0);
        assert_eq!(randomizer.gen_pure(), 1);
    }
}
